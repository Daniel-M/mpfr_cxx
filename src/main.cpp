#include "mpfr_cxx.hpp"

#include <iostream>
#include <ostream>
#include <string>

int main(void)
{
    mpfr_t number;
    mpfr_exp_t exponent;

    char text[] = "0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001";
    std::string stext(text);

    char *strnumber = (char*) malloc( sizeof(*strnumber)*256 + 1); 

    //mpfr_init_set_str(number,text,10,MPFR_RNDN);
   
    gimmie_numbah(text,number);

    mpfr_printf ("%.128Rf\n", number);
    mpfr_printf ("%.128RNf\n", number);
    mpfr_printf ("%.128R*f\n", MPFR_RNDN, number);

    mpfr_get_str(strnumber,&exponent,10,0,number,MPFR_RNDN);
    
    std::cout << exponent << std::endl; 
    std::cout << strnumber << std::endl; 
    
    mpfr_sprintf(strnumber,"%.128Rf",number);
    std::cout << strnumber << std::endl; 

    std::cout << text << std::endl;
    std::cout << *text << std::endl;
    std::cout << &text << std::endl;
    
    std::cout << "MPFR_CXX" << std::endl;

    //mpfr_cxx num(10,300);
    mpfr_cxx num1(10,256,text);
    //mpfr_cxx num2(10,256,"0.0000000000000000000000000000000000001");
    mpfr_cxx num2(10,256,"0.0000000000000000000000000000000000001");

    //std::cout << num1 << std::endl;
    std::cout << num2 << std::endl;
    num2 += num2;
    //std::cout << num1 << std::endl;
    std::cout << num2 << std::endl;
    num2++;
    std::cout << num2 << std::endl;

    //num2.get_mpfr_t(number);
    //mpfr_printf ("%.128Rf\n", number);

    //free(strnumber);
    return 0;
}
